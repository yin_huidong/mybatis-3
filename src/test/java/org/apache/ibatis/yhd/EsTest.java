/*
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.yhd;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author 二十
 * @since 2021/11/22 8:41 上午
 */
public class EsTest {
  /**
   * 字节流
   */
  private InputStream in;
  /**
   * 数据库连接工厂
   */
  private SqlSessionFactory sqlSessionFactory;
  /**
   * 数据库连接
   */
  private SqlSession sqlSession;

  {
    try {

      //将配置文件以二进制字节流的方式加载到内存
      in = Resources.getResourceAsStream("/Users/yhd/Project/IdeaProject/mybatis-3/src/test/java/resources/mybatis-config.xml");
      //通过构建者模式创建一个数据库连接工厂
      sqlSessionFactory = new SqlSessionFactoryBuilder().build(in);
      //通过工厂来管理和获取数据库连接
      sqlSession = sqlSessionFactory.openSession();
    } catch (IOException e) {

      e.printStackTrace();
    }
  }
  @Test
  public void query() {
    //获取一个指定类型的mapper代理对象
    UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
    //代理对象执行目标方法
    List<Object> users = userMapper.queryUser(1L);
    users.forEach(System.out::println);
  }


  private static interface UserMapper {
    List<Object> queryUser(Long id);
  }

  /**
   * 利用对象的finalize 方法进行资源回收
   * @throws Throwable
   */
  @Override
  protected void finalize() throws Throwable {
    sqlSession.commit(true);
    assert sqlSession != null;
    sqlSession.close();
    assert in != null;
    try {
      in.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}


