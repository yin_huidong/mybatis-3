/*
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.scripting.xmltags;

import org.apache.ibatis.builder.SqlSourceBuilder;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.session.Configuration;

/**
 * @author Clinton Begin
 */
public class DynamicSqlSource implements SqlSource {

  private final Configuration configuration;
  private final SqlNode rootSqlNode;

  /**
   * 不论是RawSqlSource还是DynamicSqlSource，都会将getBoundSql的方法委托给内部的StaticSqlSource对象。
   * 但是对比RawSqlSource和DynamicSqlSource的字段值，可以很直观的发现RawSqlSource直接有一个SqlSource属性，
   * 构造函数中通过configuration和SqlNode直接解析SqlSource对象，
   *
   * 而DynamicSqlSource相反，他没有SqlSource属性，反而是保留了configuration和SqlNode作为属性，
   * 只有在getBoundSql时，才会去创建SqlSource对象。
   *
   * 这正是因为动态Sql的sqlsource是无法直接确定的，需要在运行时根据条件才能确定。
   *
   * 所以，对于动态SQL的解析其实是分为两阶段的：
   *
   * 1.解析XML资源：之前的解析过程都类似，XMLScriptBuilder会将XML中的节点解析成各个类型的SqlNode，然后封装成MixedSqlNode，
   * 它和Configuration对象一起作为参数，创建DynamicSqlSource对象。
   *
   * 2.执行SQL：，在Executor在执行SQL时，会通过MappedStatement对象获取BoundSql对象，
   * 而MappedStatement对象是把这一操作委托给SqlSource。
   * 因此，这时候DynamicSqlSource才会真的执行getBoundSql方法，解析得到BoundSql对象。
   * @param configuration
   * @param rootSqlNode
   */
  public DynamicSqlSource(Configuration configuration, SqlNode rootSqlNode) {
    this.configuration = configuration;
    this.rootSqlNode = rootSqlNode;
  }

  @Override
  public BoundSql getBoundSql(Object parameterObject) {
    //传入configuration和运行时的参数，创建DynamicContext对象
    DynamicContext context = new DynamicContext(configuration, parameterObject);
    //应用每个SqlNode，拼接Sql片段，这里只替换动态部分
    //此时context的sqlBuilder已经被解析成了:select * from xxx where id =
    rootSqlNode.apply(context);
    SqlSourceBuilder sqlSourceParser = new SqlSourceBuilder(configuration);
    //继续解析SQL，将#{}替换成?
    Class<?> parameterType = parameterObject == null ? Object.class : parameterObject.getClass();
    //创建BoundSql对象
    // DynamicContext 对象 ，其实可以将它理解成 SQL片段的一个容器 ， 用于之后拼接出SQL。同时他还有一个bindings属性，可以用来保存
    //运行信息，比如绑定的参数，数据库id等等。
    SqlSource sqlSource = sqlSourceParser.parse(context.getSql(), parameterType, context.getBindings());
    BoundSql boundSql = sqlSource.getBoundSql(parameterObject);
    context.getBindings().forEach(boundSql::setAdditionalParameter);
    //到了这里，其实我们就已经获得了动态的 boundSql ，剩下的过程和解析静态的SQL是一样的。
    return boundSql;
  }

}
