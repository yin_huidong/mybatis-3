/*
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.mapping;

/**
 * 表示从 XML 文件或注释读取的映射语句的内容。 它根据从用户收到的输入参数创建将传递到数据库的 SQL。
 * @author 二十
 */
public interface SqlSource {
  /**
   * 实现类很多   需要我们重点关注的是   StaticSqlSource，RawSqlSource和DynamicSqlSource
   * 补充：Mybatis动态SQL和静态SQL的区别
   * 1.动态SQL表示这个SQL节点中含有${}或是其他动态的标签（比如，if，trim，foreach，choose，bind节点等），需要在运行时根据传入的条件才能确定SQL，因此对于动态SQL的MappedStatement的解析过程应该是在运行时。
   * 2.静态SQL是不含以上这个节点的SQL，能直接解析得到含有占位符形式的SQL语句，而不需要根据传入的条件确定SQL，因此可以在加载时就完成解析。所在在执行效率上要高于动态SQL。
   * DynamicSqlSource和RawSqlSource就分别对应了动态SQL和静态SQL，它们都封装了StaticSqlSource。
   * @param parameterObject
   * @return
   */
  BoundSql getBoundSql(Object parameterObject);

}
